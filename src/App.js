import React from 'react';
import style from './App.css';
import axios from 'axios';

export default class ImageComponent extends React.Component {
  state = {
      imageData: []
  }

  componentDidMount() {
      axios.get('https://jsonplaceholder.typicode.com/photos')
          .then( res => {
              const imageData = res.data.splice(0, 50);
              this.setState({ imageData });
          } )
  }

  render() {
      return (
          <div className={ style.container }>
              {
                  this.state.imageData.map( ( data, i ) => {
                    return (
                      <div key={ i }>
                        <img src={ data.url } title={ data.title } alt={ data.title }  />
                      </div>
                    )
                  } )
              }
          </div>
      )
  }
}
